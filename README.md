# Deploy Ingress Controller
**1. Deploy Ingress Controller for NodePort**
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.1/deploy/static/provider/baremetal/deploy.yaml
```
OR 
```
kubectl apply -f deploy-np.yaml
```

More: https://platform9.com/learn/v1.0/tutorials/nodeport-ingress


**2. Deploy Ingress Controller for LoadBalancer**
```
kubectl apply -f deploy-lb.yaml
```
_NOTE:_ set External IP inside .yaml

<br />
<br />
<br />

# Deploy Ingress Resource
**1. Deploy APP**
```
kubectl create ns rocky-app

kubectl create deployment webapp-rocky1 --image=petyb/rocky:1 --replicas=3 -n rocky-app

kubectl expose deployment webapp-rocky1 -n rocky-app --name rocky1-service --port 8080 --target-port=80
```

**2. Deploy Ingress Resource**
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-rocky-app
  namespace: rocky-app
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
spec:
  ingressClassName: nginx
  rules:
  - host: "rocky.onmetal.sk"
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: rocky1-service
            port:
              number: 8080
```

**3. Access App**
- NodePort:
http://rocky.onmetal.sk:30573/
