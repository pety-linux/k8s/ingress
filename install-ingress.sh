#!/bin/bash
# Variables
ip=`hostname -I | awk '{print $1}'`

# set ip
sed -i "s/10.10.10.10/$ip/g" deploy-lb.yaml

# apply
kubectl apply -f deploy-lb.yaml
